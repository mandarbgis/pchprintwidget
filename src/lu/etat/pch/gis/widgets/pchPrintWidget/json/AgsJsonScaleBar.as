package lu.etat.pch.gis.widgets.pchPrintWidget.json {
	/*
	http://help.arcgis.com/en/sdk/10.0/java_ao_adf/api/arcobjects/com/esri/arcgis/carto/IScaleBar.html
	*/
    public class AgsJsonScaleBar extends AgsJsonSymbol {
		private var _scaleBarType:String;
		private var _barHeight:Number;
		private var _units:String;
		private var _barColor:AgsJsonRGBColor;
		private var _division:Number;
		private var _divisions:Number;
		private var _divisionsBeforeZero:Number;
		private var _labelFrequency:Number;
		private var _labelPosition:Number;
		private var _labelGap:Number;
		private var _labelSymbol:AgsJsonTextSymbol;
		private var _numberFormat:AgsJsonNumericFormat;
		private var _resizeHint:Number;
		private var _subdivisions:Number;
		private var _unitLabel:String;
		private var _unitLabelGap:Number;
		private var _unitLabelPosition:Number;
		private var _unitLabelSymbol:AgsJsonTextSymbol;
		private var _backgroundColor:AgsJsonRGBColor;

        public function AgsJsonScaleBar(barHeight:Number=5,scaleBarType:String="AlternatingScaleBar") {
            type="pchScaleBar";
            this.barHeight=barHeight;
			this.scaleBarType=scaleBarType;
        }

		/** 
			valid scaleBarType values:
			 * AlternatingScaleBar
			 * DoubleAlternatingScaleBar
			 * HollowScaleBar
			 * ScaleLine
			 * SingleDivisionScaleBar
			 * SteppedScaleLine
		*/
		public function get scaleBarType():String
		{
			return _scaleBarType;
		}
		
		public function set scaleBarType(value:String):void
		{
			_scaleBarType = value;
		}

		public function get barHeight():Number {
            return _barHeight;
        }

        public function set barHeight(value:Number):void {
            _barHeight=value;
        }

		public function get units():String
		{
			return _units;
		}

		public function set units(value:String):void
		{
			_units = value;
		}

		public function get barColor():AgsJsonRGBColor
		{
			return _barColor;
		}

		public function set barColor(value:AgsJsonRGBColor):void
		{
			_barColor = value;
		}

		public function get division():Number
		{
			return _division;
		}

		public function set division(value:Number):void
		{
			_division = value;
		}

		public function get divisions():Number
		{
			return _divisions;
		}

		public function set divisions(value:Number):void
		{
			_divisions = value;
		}

		public function get divisionsBeforeZero():Number
		{
			return _divisionsBeforeZero;
		}

		public function set divisionsBeforeZero(value:Number):void
		{
			_divisionsBeforeZero = value;
		}

		public function get labelFrequency():Number
		{
			return _labelFrequency;
		}

		public function set labelFrequency(value:Number):void
		{
			_labelFrequency = value;
		}

		public function get labelPosition():Number
		{
			return _labelPosition;
		}

		public function set labelPosition(value:Number):void
		{
			_labelPosition = value;
		}

		public function get labelGap():Number
		{
			return _labelGap;
		}

		public function set labelGap(value:Number):void
		{
			_labelGap = value;
		}

		public function get labelSymbol():AgsJsonTextSymbol
		{
			return _labelSymbol;
		}

		public function set labelSymbol(value:AgsJsonTextSymbol):void
		{
			_labelSymbol = value;
		}

		public function get numberFormat():AgsJsonNumericFormat
		{
			return _numberFormat;
		}

		public function set numberFormat(value:AgsJsonNumericFormat):void
		{
			_numberFormat = value;
		}

		public function get resizeHint():Number
		{
			return _resizeHint;
		}

		public function set resizeHint(value:Number):void
		{
			_resizeHint = value;
		}

		public function get subdivisions():Number
		{
			return _subdivisions;
		}

		public function set subdivisions(value:Number):void
		{
			_subdivisions = value;
		}

		public function get unitLabel():String
		{
			return _unitLabel;
		}

		public function set unitLabel(value:String):void
		{
			_unitLabel = value;
		}

		public function get unitLabelGap():Number
		{
			return _unitLabelGap;
		}

		public function set unitLabelGap(value:Number):void
		{
			_unitLabelGap = value;
		}

		public function get unitLabelPosition():Number
		{
			return _unitLabelPosition;
		}

		public function set unitLabelPosition(value:Number):void
		{
			_unitLabelPosition = value;
		}

		public function get unitLabelSymbol():AgsJsonTextSymbol
		{
			return _unitLabelSymbol;
		}

		public function set unitLabelSymbol(value:AgsJsonTextSymbol):void
		{
			_unitLabelSymbol = value;
		}

		public function get backgroundColor():AgsJsonRGBColor
		{
			return _backgroundColor;
		}

		public function set backgroundColor(value:AgsJsonRGBColor):void
		{
			_backgroundColor = value;
		}

    }
}