package lu.etat.pch.gis.widgets.pchPrintWidget.json
{
	public class AgsJsonMapGrid extends AgsJsonSymbol
	{
		private var _gridType:String = "measured";
		private var _xIntervalSize:Number;
		private var _yIntervalSize:Number;
		private var _labelFormat:AgsJsonGridLabelFormat;
		private var _labelAxisTop:Boolean;
		private var _labelAxisLeft:Boolean;
		private var _labelAxisRight:Boolean;
		private var _labelAxisBottom:Boolean;
		private var _spatialReferenceWKID:Number;
		private var _lineSymbol:AgsJsonSimpleLineSymbol;
		private var _xOrigin:Number;
		private var _yOrigin:Number;
		private var _label:String;
		private var _tickMarkSymbol:AgsJsonSimpleMarkerSymbol;
		private var _border:AgsJsonSimpleLineSymbol;


		public function AgsJsonMapGrid(gridType:String="measured",xIntervalSize:Number=1000,yIntervalSize:Number=1000,labelAxisTop:Boolean=true,labelAxisLeft:Boolean=true,labelAxisRight:Boolean=true,labelAxisBottom:Boolean=true,spatialReferenceWKID:Number=0,label:String="MapGrid")
		{
			type="pchMapGrid";
			this.gridType=gridType;
			this.xIntervalSize=xIntervalSize;
			this.yIntervalSize=yIntervalSize;
			this.labelAxisTop=labelAxisTop;
			this.labelAxisLeft=labelAxisLeft;
			this.labelAxisRight=labelAxisRight;
			this.labelAxisBottom=labelAxisBottom;
			this.spatialReferenceWKID=spatialReferenceWKID;
			this.label=label;
		}

		public function get labelFormat():AgsJsonGridLabelFormat
		{
			return _labelFormat;
		}

		public function set labelFormat(value:AgsJsonGridLabelFormat):void
		{
			_labelFormat = value;
		}

		public function get labelAxisBottom():Boolean
		{
			return _labelAxisBottom;
		}

		public function set labelAxisBottom(value:Boolean):void
		{
			_labelAxisBottom = value;
		}

		public function get labelAxisRight():Boolean
		{
			return _labelAxisRight;
		}

		public function set labelAxisRight(value:Boolean):void
		{
			_labelAxisRight = value;
		}

		public function get labelAxisLeft():Boolean
		{
			return _labelAxisLeft;
		}

		public function set labelAxisLeft(value:Boolean):void
		{
			_labelAxisLeft = value;
		}

		public function get labelAxisTop():Boolean
		{
			return _labelAxisTop;
		}

		public function set labelAxisTop(value:Boolean):void
		{
			_labelAxisTop = value;
		}

		public function get yIntervalSize():Number
		{
			return _yIntervalSize;
		}

		public function set yIntervalSize(value:Number):void
		{
			_yIntervalSize = value;
		}

		public function get xIntervalSize():Number
		{
			return _xIntervalSize;
		}

		public function set xIntervalSize(value:Number):void
		{
			_xIntervalSize = value;
		}

		public function get gridType():String
		{
			return _gridType;
		}

		public function set gridType(value:String):void
		{
			_gridType = value;
		}

		public function get spatialReferenceWKID():Number
		{
			return _spatialReferenceWKID;
		}

		public function set spatialReferenceWKID(value:Number):void
		{
			_spatialReferenceWKID = value;
		}

		public function get yOrigin():Number
		{
			return _yOrigin;
		}

		public function set yOrigin(value:Number):void
		{
			_yOrigin = value;
		}

		public function get xOrigin():Number
		{
			return _xOrigin;
		}

		public function set xOrigin(value:Number):void
		{
			_xOrigin = value;
		}

		public function get label():String
		{
			return _label;
		}

		public function set label(value:String):void
		{
			_label = value;
		}

		public function get lineSymbol():AgsJsonSimpleLineSymbol
		{
			return _lineSymbol;
		}

		public function set lineSymbol(value:AgsJsonSimpleLineSymbol):void
		{
			_lineSymbol = value;
		}

		public function get tickMarkSymbol():AgsJsonSimpleMarkerSymbol
		{
			return _tickMarkSymbol;
		}

		public function set tickMarkSymbol(value:AgsJsonSimpleMarkerSymbol):void
		{
			_tickMarkSymbol = value;
		}

		public function get border():AgsJsonSimpleLineSymbol
		{
			return _border;
		}

		public function set border(value:AgsJsonSimpleLineSymbol):void
		{
			_border = value;
		}


	}
}