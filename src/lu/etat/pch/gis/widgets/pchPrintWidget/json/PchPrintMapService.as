package lu.etat.pch.gis.widgets.pchPrintWidget.json {
	import com.esri.ags.layers.ArcGISDynamicMapServiceLayer;
	import com.esri.ags.layers.ArcGISImageServiceLayer;
	import com.esri.ags.layers.ArcGISTiledMapServiceLayer;
	import com.esri.ags.layers.DynamicMapServiceLayer;
	import com.esri.ags.layers.FeatureLayer;
	import com.esri.ags.layers.Layer;
	import com.esri.ags.layers.WMSLayer;
	import com.esri.ags.layers.supportClasses.LayerInfo;
	import com.esri.ags.virtualearth.VETiledLayer;
	
	import mx.collections.ArrayCollection;
	
	import spark.components.TextArea;
	
	/**
	 * 
	 * @author schullto
	 */
	public class PchPrintMapService {
		private static var _taDebug:TextArea;
		
		
		/**
		 * 
		 * @param url
		 * @param type
		 * @param name
		 * @param visibleIds
		 * @param alpha
		 */
		public function PchPrintMapService(url:String=null, type:String="AGS", name:String="noName", visibleIds:String="*", alpha:Number=1) {
			this._url=url;
			this._type=type;
			this._name=name;
			this._visibleIds=visibleIds;
			this._alpha=alpha;
			super();
		}
		
		private var _alpha:Number;
		private var _name:String;
		private var _type:String;
		private var _url:String;
		private var _visibleIds:String;
		private var _definitionExpression:Array;
		private var _token:String;
		
		/**
		 * 
		 * @return 
		 */
		public function get alpha():Number {
			return _alpha;
		}
		
		/**
		 * 
		 * @param value
		 */
		public function set alpha(value:Number):void {
			_alpha=value;
		}
		
		/**
		 * 
		 * @return 
		 */
		public function get name():String {
			return _name;
		}
		
		/**
		 * 
		 * @param value
		 */
		public function set name(value:String):void {
			_name=value;
		}
		
		/**
		 * ArcGIS mapservice = AGS  
		 * WMS mapservice = WMS  
		 * @return 
		 */
		public function get type():String {
			return _type;
		}
		
		/**
		 * ArcGIS mapservice = AGS  
		 * WMS mapservice = WMS  
		 * @param value
		 */
		public function set type(value:String):void {
			_type=value;
		}
		
		/**
		 * 
		 * @return 
		 */
		public function get url():String {
			return _url;
		}
		
		/**
		 * 
		 * @param value
		 */
		public function set url(value:String):void {
			_url=value;
		}
		
		/**
		 * show all layers "*" 
		 * show layers by id "1,4,6,7" 
		 * @return 
		 */
		public function get visibleIds():String {
			return _visibleIds;
		}
		
		/**
		 * show all layers "*" 
		 * show layers by id "1,4,6,7" 
		 * @param value
		 */
		public function set visibleIds(value:String):void {
			_visibleIds=value;
		}
		
		public function get definitionExpression():Array {
			return _definitionExpression;
		}
		
		public function set definitionExpression(value:Array):void {
			_definitionExpression = value;
		}
		
		public static function getMapServiceFromLayer(layer:Layer):PchPrintMapService {
			var mapService:PchPrintMapService;
			if(layer is ArcGISDynamicMapServiceLayer) {
				mapService=PchPrintMapService.getMapServiceFromDynamicService(layer as ArcGISDynamicMapServiceLayer);
				if(!mapService.visibleIds || mapService.visibleIds.length==0) return null;
			} else if(layer is ArcGISTiledMapServiceLayer) {
				mapService=PchPrintMapService.getMapServiceFromTiledService(layer as ArcGISTiledMapServiceLayer);
			} else if(layer is FeatureLayer) {
				mapService=PchPrintMapService.getMapServiceFromFeatureLayer(layer as FeatureLayer);
			} else if(layer is ArcGISImageServiceLayer) {
				mapService=PchPrintMapService.getMapServiceFromImageService(layer as ArcGISImageServiceLayer);
			} else if(layer is VETiledLayer) {
				mapService=PchPrintMapService.getMapServiceFromVirtualEarthService(layer as VETiledLayer);
			} else if(layer is WMSLayer) {
				mapService=PchPrintMapService.getMapServiceFromWMSService(layer as com.esri.ags.layers.WMSLayer);
			}
			return mapService;
		}
		
		public static function getMapServiceFromDynamicService(layer:ArcGISDynamicMapServiceLayer):PchPrintMapService {
			var mapService:PchPrintMapService = new PchPrintMapService(layer.url,"AGS",layer.name);
			doDebug("layer["+layer.name+"].visibleLayers: "+layer.visibleLayers);
			if(layer.visibleLayers)  { 
				var visibleLayersWithParentVisibility:ArrayCollection = new ArrayCollection();
				var visibleLayer:ArrayCollection = layer.visibleLayers;
				for each (var visibleLayerId:Number in visibleLayer) {
					var layerInfo:LayerInfo = layer.layerInfos[visibleLayerId];
					var allParentsVisible:Boolean=checkAllParentsVisible(layerInfo,layer.layerInfos,visibleLayer);
					doDebug("layer["+layerInfo.id+"].allParentsVisible: "+allParentsVisible);
					if(allParentsVisible) {
						if(layerInfo.subLayerIds) {
							doDebug("layer["+layerInfo.id+"].hasSubLayerIds -> IGNORED");
							//ignore GroupLayers
						}else{
							doDebug("layer["+layerInfo.id+"].subLayerIds.EMPTY.addForPrint: "+visibleLayerId);
							visibleLayersWithParentVisibility.addItem(visibleLayerId);
						}
					}
				}
				doDebug("visibleIds[visibleLayersNotNull]: "+visibleLayersWithParentVisibility.toArray().toString());
				mapService.visibleIds = visibleLayersWithParentVisibility.toArray().toString();
			}else{
				var defaultVisibleLayers:ArrayCollection = new ArrayCollection();
				var rootLayers:Array = findRootLayers(layer.layerInfos);
				doDebug("rootLayers: "+rootLayers);
				for each (var layerInfo1:LayerInfo in rootLayers) {
					doDebug("layerInfo["+layerInfo1.id+"]: "+layerInfo1.name+"  defaultVisibility="+layerInfo1.defaultVisibility+"  subLayerIds="+layerInfo1.subLayerIds);
					if(layerInfo1.defaultVisibility) {
						if(layerInfo1.subLayerIds) {
							var childsVisibility:ArrayCollection = getChildsVisibility(layerInfo1,layer.layerInfos);
							doDebug("layerInfo["+layerInfo1.id+"].childsVisibility.Added: "+childsVisibility.toArray().toString());
							if(childsVisibility.length>0) {
								defaultVisibleLayers.addAll(childsVisibility);
							}
						}else{
							doDebug("layerInfo["+layerInfo1.id+"].noSubLayer.AddMe");
							defaultVisibleLayers.addItem(layerInfo1.id);
						}
					}
				}
				mapService.visibleIds=defaultVisibleLayers.toArray().toString();
				doDebug("visibleIds[visibleLayersNull]: "+defaultVisibleLayers.toArray().toString());
			}
			if(layer.layerDefinitions) {
				mapService.definitionExpression = layer.layerDefinitions;
			}
			mapService.token=layer.token;
			mapService.alpha = layer.alpha;
			return mapService;
		}
		
		private static function checkAllParentsVisible(layerInfo:LayerInfo, layerInfos:Array, visibleLayer:ArrayCollection):Boolean {
			if (!isNaN(layerInfo.parentLayerId) && (layerInfo.parentLayerId > -1)) {
				var parentLayerId:Number = layerInfo.parentLayerId;
				if(visibleLayer.getItemIndex(parentLayerId)>-1) {
					//parent is visible
					return checkAllParentsVisible(layerInfos[parentLayerId],layerInfos,visibleLayer);
				}
				return false;
			}
			return true;
		}
		
		private static function getChildsVisibility(layerInfo:LayerInfo,layerInfos:Array):ArrayCollection {
			var childsVisibility:ArrayCollection = new ArrayCollection();
			for each(var childId:Number in layerInfo.subLayerIds) {
				var childLayerInfo:LayerInfo = layerInfos[childId];
				if(childLayerInfo.defaultVisibility) {
					if(childLayerInfo.subLayerIds) {
						var childChildsVisibility:ArrayCollection = getChildsVisibility(childLayerInfo,layerInfos);
						if(childChildsVisibility.length>0) {
							childsVisibility.addAll(childChildsVisibility);
						}
					}else{
						childsVisibility.addItem(childLayerInfo.id);
					}
				}
			}
			return childsVisibility;
		}
		
		private static function findRootLayers(layerInfos:Array):Array { // of LayerInfo
			var roots:Array = [];
			for each (var layerInfo:LayerInfo in layerInfos) {
				// ArcGIS: parentLayerId is -1
				// ArcIMS: parentLayerId is NaN
				if (isNaN(layerInfo.parentLayerId) || layerInfo.parentLayerId == -1) {
					roots.push(layerInfo);
				}
			}
			return roots;
		}
		
		public static function isParentLayerVisible(layer:ArcGISDynamicMapServiceLayer,childLayerId:Number):Boolean {
			var layerInfos:Array=layer.layerInfos;
			var childLayerInfo:LayerInfo =  layerInfos[childLayerId];
			if(childLayerInfo.parentLayerId>=0) {
				var parentLayerInfo:LayerInfo =  layerInfos[childLayerInfo.parentLayerId];
				if(layer.visibleLayers.getItemIndex(parentLayerInfo.id)>-1) 
					return isParentLayerVisible(layer,parentLayerInfo.id);
				else return false;
				
			} else return true;
		}
		
		public static function getMapServiceFromTiledService(layer:ArcGISTiledMapServiceLayer):PchPrintMapService {
			var mapService:PchPrintMapService = new PchPrintMapService(layer.url,"AGS",layer.name);
			mapService.token=layer.token;
			mapService.alpha = layer.alpha;
			return mapService;
		}
		public static function getMapServiceFromImageService(layer:ArcGISImageServiceLayer):PchPrintMapService {
			var mapService:PchPrintMapService = new PchPrintMapService(layer.url,"AGS",layer.name);
			mapService.token=layer.token;
			mapService.alpha = layer.alpha;
			return mapService;
		}
		public static function getMapServiceFromVirtualEarthService(layer:VETiledLayer):PchPrintMapService {
			var mapService:PchPrintMapService = new PchPrintMapService("http://bing.com","BING",layer.name,layer.mapStyle);
			mapService.alpha = layer.alpha;
			return mapService;
		}
		public static function getMapServiceFromFeatureLayer(layer:FeatureLayer):PchPrintMapService {
			var url:String = layer.url;
			var posi:Number =url.lastIndexOf("/FeatureServer/");
			var mapService:PchPrintMapService;
			if(posi>0) {
				var layerId:String = url.substr(posi+15);
				var featureServerURL:String =url.substr(0,posi)+"/MapServer"; //remove FeatureServce, add MapService
				mapService = new PchPrintMapService(featureServerURL,"AGS",layer.name,layerId);
			}else{
				posi =url.lastIndexOf("/MapServer/");
				if(posi>0) {
					var mapServerURL:String =url.substr(0,posi)+"/MapServer";
					mapService = new PchPrintMapService(mapServerURL,"AGS",layer.name,layer.layerDetails.id+"");
				}else{
					posi =url.lastIndexOf("/FeatureServer");
					if(posi>0) {
						var featureServerURL2:String =url.substr(0,posi)+"/MapServer"; //remove FeatureServce, add MapService
						mapService = new PchPrintMapService(featureServerURL2,"AGS",layer.name,layer.layerDetails.id+"");
					}
					mapService = new PchPrintMapService(url,"AGS",layer.name,layer.layerDetails.id+"");
				}
			}
			var defExpArray:Array = new Array();
			defExpArray.push(layer.definitionExpression);
			mapService.definitionExpression = defExpArray;
			mapService.alpha = layer.alpha;
			mapService.token=layer.token;
			return mapService;
		}
		public static function getMapServiceFromWMSService(layer:com.esri.ags.layers.WMSLayer):PchPrintMapService {
			var mapService:PchPrintMapService = new PchPrintMapService(layer.url,"WMS",layer.name,layer.visibleLayers.toArray().toString());
			mapService.alpha = layer.alpha;
			return mapService;
		}
		
		public function get token():String
		{
			return _token;
		}
		
		public function set token(value:String):void
		{
			_token = value;
		}

		public static function get taDebug():TextArea
		{
			return _taDebug;
		}

		public static function set taDebug(value:TextArea):void
		{
			_taDebug = value;
		}


		protected static function doDebug(message:String,alert:Boolean=false):void {
			trace(message);
			if(taDebug)
				taDebug.text=taDebug.text+"\n"+message;
	
		}
	}
	

}