package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    public class AgsJsonTextElement extends AgsJsonSymbol {

        public function AgsJsonTextElement(text:String="",textSymbol:AgsJsonTextSymbol=null) {
            type="pchTextElement";
			_text=text;
			_textSymbol=textSymbol;
        }

		private var _text:String;
		private var _textSymbol:AgsJsonTextSymbol;


		public function get text():String
		{
			return _text;
		}

		public function set text(value:String):void
		{
			_text = value;
		}


		public function get textSymbol():AgsJsonTextSymbol
		{
			return _textSymbol;
		}

		public function set textSymbol(value:AgsJsonTextSymbol):void
		{
			_textSymbol = value;
		}


    }
}