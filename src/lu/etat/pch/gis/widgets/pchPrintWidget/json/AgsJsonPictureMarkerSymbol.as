package lu.etat.pch.gis.widgets.pchPrintWidget.json {
    import com.esri.ags.symbols.PictureMarkerSymbol;
    
    import flash.external.ExternalInterface;
    
    import mx.managers.BrowserManager;

    /**
     *
     * @author schullto
     */
    public class AgsJsonPictureMarkerSymbol extends AgsJsonSymbol {
		private var _url:String;
		private var _width:Number;
		private var _height:Number;
		private var _angle:Number;
		private var _xOffset:Number;
		private var _yOffset:Number;

        /**
         *
         * @param url
         * @param width
         * @param height
         * @param angle
         * @param xOffset
         * @param yOffset
         */
        public function AgsJsonPictureMarkerSymbol(url:String,width:Number,height:Number,angle:Number=0,xOffset:Number=0,yOffset:Number=0) {
            type="esriPMS";
			this.url = url;
			this.width=width;
			this.height=height;
			this.angle=angle;
			this.xOffset=xOffset;
			this.yOffset=yOffset;
			
        }
		
		public static function getCurrentWebpage():String {
			var url:String = ExternalInterface.call('eval','document.location.protocol')+"//"+ExternalInterface.call('eval','document.location.hostname');
			var port:String =ExternalInterface.call('eval','document.location.port');
			if(port) url=url+":"+port;
			trace("getCurrentWebpage.url: "+url);
			url = url+ExternalInterface.call('eval','document.location.pathname');
			url = url.substr(0,url.lastIndexOf("/"));
			return url;
		}
		

        public static function getFromObject(pms:PictureMarkerSymbol):AgsJsonPictureMarkerSymbol {
            if(pms==null)
                return null;
            var url:String=pms.source+"";
			if(url.substr(0,7)!='http://') {
				url = getCurrentWebpage()+"/"+url;
			}
			trace("AgsJsonPictureMarkerSymbol.getFromObject.url: "+url);
			var width:Number = pms.width;
			var height:Number = pms.height;
            var retSym:AgsJsonPictureMarkerSymbol=new AgsJsonPictureMarkerSymbol(url,width,height,pms.angle,pms.xoffset,pms.yoffset);
            return retSym;
        }

		public function get url():String
		{
			return _url;
		}

		public function set url(value:String):void
		{
			_url = value;
		}

		public function get width():Number
		{
			return _width;
		}

		public function set width(value:Number):void
		{
			_width = value;
		}

		public function get height():Number
		{
			return _height;
		}

		public function set height(value:Number):void
		{
			_height = value;
		}

		public function get angle():Number
		{
			return _angle;
		}

		public function set angle(value:Number):void
		{
			_angle = value;
		}

		public function get xOffset():Number
		{
			return _xOffset;
		}

		public function set xOffset(value:Number):void
		{
			_xOffset = value;
		}

		public function get yOffset():Number
		{
			return _yOffset;
		}

		public function set yOffset(value:Number):void
		{
			_yOffset = value;
		}


    }
}